let { makeDirectory, createFilesInDir, deleteFilesInDir } = require("../problem1.cjs");

let folderName = "JSON test folder"
let fullDir = `/home/shasidhar/Desktop/Mountblue drills/JavaScript Assignment/fscallbacks/${folderName}`;
let generateFilesCount = 5;
let deleteFilesCount = 5;

makeDirectory(fullDir, (dir) => {
    createFilesInDir(dir, generateFilesCount, (dir) => {
        deleteFilesInDir(dir,deleteFilesCount);
    })
})